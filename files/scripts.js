function play(str){
  if (str==""){
	document.getElementById("game").innerHTML="";
	return;
  } 
  if (window.XMLHttpRequest)
	xmlhttp=new XMLHttpRequest();
  else
	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");

  xmlhttp.onreadystatechange=function()
	{
		if (xmlhttp.readyState==4 && xmlhttp.status==200)
			document.getElementById("game").innerHTML=xmlhttp.responseText;
	}
	xmlhttp.open("GET","type.php?cat="+str,true);
	xmlhttp.send();
}

function guess(str){
  letter = document.getElementById(str);
  letter.removeAttribute('href');
  
  if (str==""){
	document.getElementById("guess").innerHTML="";
	return;
  } 
  if (window.XMLHttpRequest)
	xmlhttp=new XMLHttpRequest();
  else
	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");

  xmlhttp.onreadystatechange=function()
	{
		if (xmlhttp.readyState==4 && xmlhttp.status==200)
			document.getElementById("guess").innerHTML+=xmlhttp.responseText;
	}
	xmlhttp.open("GET","guess.php?letter="+letter.id,true);
	xmlhttp.send();
}
